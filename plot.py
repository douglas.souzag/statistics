import matplotlib.pyplot as plt
#https://www.youtube.com/watch?v=ZqlgLMMOiRU
def regressaoLinear(caminho_arq):
    x = []
    y = []
    x_vezes_y = []
    x_ao_quadrado = []
    
    soma_x = 0
    soma_y = 0
    soma_x_vezes_y = 0
    soma_x_ao_quadrado = 0

    dataset = open(caminho_arq, 'r')
    header = open(caminho_arq, 'r')
    head = header.readlines()

    #CRIANDO RETA COM GRAFICO
    cont = 0
    for linha in dataset:
        cont = cont + 1
        if cont == 1:
            continue
        linha.strip()
        X = linha.split(',')[0]
        x.append(float(X))
        Y = linha.split(',')[1]
        y.append(float(Y))
        
    dataset.close()   
    plt.scatter(x,y)
    plt.title(caminho_arq)
    plt.xlabel(head[0].split(',')[0])
    plt.ylabel(head[0].split(',')[1])
    plt.show()

    
    #PEGANDO RETA DE REGRESSÃO

    #SOMANDO TODOS OS ELEMENTOS DA COLUNA X
    for m in x:
        soma_x = soma_x + round(float(m),2)
        
    #SOMANDO TODOS OS ELEMENTOS DA COLUNA Y
    for m in y:
        soma_y = soma_y + round(float(m),2)
        
    #POPULANDO A TABELA X.Y
    for m,n in zip(x,y):
        x_vezes_y.append(round(float(m)*float(n),2))
        
    #SOMANDO ELEMENTOS DA TABELA X.Y
    for m in x_vezes_y:
        soma_x_vezes_y = round(soma_x_vezes_y + float(m),2)
        
    #POPULANDO TABEL DE X²
    for m in x:
        x_ao_quadrado.append(round(float(m)**2,2))
        
    #SOMANDO ELEMENTOS DE X²
    for m in x_ao_quadrado:
        soma_x_ao_quadrado = soma_x_ao_quadrado + round(float(m),2)
        
    print('\nColuna X: ',x)
    print('Total X: ',soma_x)
    print('\nColuna Y: ',y)
    print('Total Y: ',soma_y)
    print('\nColuna X.Y: ',x_vezes_y)
    print('Total X.Y: ',soma_x_vezes_y)
    print('\nColuna X²: ',x_ao_quadrado)
    print('Total X²: ',soma_x_ao_quadrado)
    
    #PEGANDO VALOR DO ALPHA
    alpha = ((len(x)*soma_x_vezes_y)-(soma_x*soma_y)) / ((5*soma_x_ao_quadrado) - soma_x**2)
    print('\nAlpha: %.2f' %(alpha))

    #PEGANDO VALOR DO BETA
    
    beta = (soma_y / len(x) - (alpha*(soma_x / len(x))))
    print('\nBeta: %.2f' %(beta))

    #RETA DE REGRESSÃO

    print('\nY = ',round(alpha,2),'X - ',round(beta,2),' + erro')


regressaoLinear('teste.csv')
