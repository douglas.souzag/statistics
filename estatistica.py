#Faculdade FATEC São José dos Campos - Professor: Dawilmar
#Script criado por: Douglas de Souza
#Bibliotecas usadas: math,os(para leitura de arquivos e limpar a tela),PyQt5(para interface)
#Funçôes criadas: media,mediana,moda,amplitude,variancia,desvioPadrao,coeficienteVariacao.

from os import system
import math

#Calculo de média
def media(lista): #<- Recebe como argumento uma lista.
    tam_lista = len(lista) #<- Retorna o tamanho da lista.
    soma = 0
    for x in lista: #<- Para cada item da lista, ele adiciona na variavel 'soma'.
        soma = soma + x
    return soma/tam_lista #<- Retorna a soma dividida pelo tamanho da lista.

#Calculo de mediana
def mediana(lista):
    tam_lista = len(lista) 
    if(tam_lista % 2 == 0): #<- Se o tamanho da lista for par.
        num1 = lista[int(tam_lista/2)] #<-Pega o primeiro numero no meio da lista
        num2 = lista[int((tam_lista/2)-1)] #<- Pega o segundo numero no meio da lista
        return (num1 + num2) / 2 #Retorna a soma entre eles
    else: #Se o numero de elementos for impar.
        num1 = lista[int((tam_lista)/2)]
        return num1 #Retorna o elemento do meio.

#Calculo de moda
def moda(lista):
    lista_dic = {} #<- Criando dicionario vazio para guardar as repetições.
    for x in lista: #<- Para cada elemento da lista, se não estiver no dicionario, adicionar no indice do elemento o numero 1, se estiver, somar com mais 1.
        if x not in lista_dic:
            lista_dic[x] = 1
        else:
            lista_dic[x] += 1
    maior_repeticao = max(lista_dic.values()) #<- Pega o elemento que maior valor no dicionario.

    for y in lista_dic: #<- Para conseguir o indice, compara o maior valor com todos os elementos do dicionario e retorna a moda se o elemento for igual.
        if lista_dic[y] == maior_repeticao:
            moda = y
    return moda



#Calculo de amplitude
def amplitude(lista):
    lista_ordenada = sorted(lista) #<- Ordena a lista em ordem crescente
    return lista_ordenada[-1] - lista_ordenada[0] #<- Retorna a subtração do primeiro elemento da lista(menor) pelo ultimo elemento da lista(maior).

#Calculo de variância
def variancia(lista):
    media_lista = media(lista) #<- Retorna a média da lista, (função criada anteriormente).
    lista_somar = []
    soma = 0
    for x in lista: #<- Para cada item da lista, ele subtrai a média, eleva ao quadrado e então adiciona na lista 'lista_somar'.
        lista_somar.append((x-media_lista)**2)
    for x in lista_somar: #<- Para cada item da lista_somar ele acrescenta na variavel 'soma'.
        soma = soma+x
    return soma/len(lista) #<- Retorna a soma dos elementos da lista_somar divididos pelo numero de elementos.

#Calculo de desvio padrão
def desvioPadrao(lista):
    variancia_lista = variancia(lista) #<- Retorna a variância da lista, (função criada anteriormente).
    return math.sqrt(variancia_lista) #<- Retorna a raiz quadrada da variancia.

#Calculo de coeficiente de variação
def coeficienteVariacao(lista):
    desvio_padrao_lista = desvioPadrao(lista) #<- Retorna o desvio padrão da lista (função criada anteriormente)
    media_lista = media(lista) #<- Retorna a média da lista, (função criada anteriormente).

    return (desvio_padrao_lista*100)/media_lista #<- Retorna o coeficiente de variação, que é o desvio padrão multiplicado por 100 e depois dividido pela média da lista

while True:
    x=0
    l = []
    linhas = int(input('Numero de elementos:'))
    l = [int(input('E%d: ' % (x+1))) for x in range(linhas)]
    opc = 0
    print('Elementos: ',l)
    opc = int(input(
        'Selecione o que deseja calcular:\n'
        '1 - Média\n'
        '2 - Mediana\n'
        '3 - Moda\n'
        '4 - Amplitude\n'
        '5 - Variância\n'
        '6 - Desvio padrão\n'
        '7 - Coeficiente de variação\n'
        '8 - Limpar\n'
        '9 - Sair\n'
    ))
    if opc == 1:
        print('A média dos elementos é: %.1f' % (media(l)))
    if opc == 2:
        print('A mediana dos elementos é: %.1f' % (mediana(l)))
    if opc == 3:
        print('A moda dos elementos é: ' , (moda(l)))
    if opc == 4:
        print('A amplitude dos elementos é: %.1f' % (amplitude(l)))
    if opc == 5:
        print('A variância dos elementos é: %.2f' % (variancia(l)))
    if opc == 6:
        print('O desvio padrão dos elementos é: %.4f' % (desvioPadrao(l)))
    if opc == 7:
        print('O coeficiente de variação dos elementos é: %.4f' % (coeficienteVariacao(l)))
    if opc == 8:
        system('clear')
    if opc == 9:
        break

